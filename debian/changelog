kdesignerplugin (5.98.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.98.0).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 18 Sep 2022 23:11:44 +0200

kdesignerplugin (5.97.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.97.0).

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 14 Aug 2022 18:55:35 +0200

kdesignerplugin (5.96.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.96.0).
  * Bump Standards-Version to 4.6.1, no change required.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 31 Jul 2022 13:33:04 +0200

kdesignerplugin (5.94.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.94.0).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 19 May 2022 23:59:05 +0200

kdesignerplugin (5.93.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.93.0).

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 11 May 2022 23:22:46 +0200

kdesignerplugin (5.90.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.89.0).

  [ Aurélien COUDERC ]
  * New upstream release (5.90.0).
  * Added myself to the uploaders.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 11 Feb 2022 23:46:43 +0100

kdesignerplugin (5.88.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.87.0).
  * New upstream release (5.88.0).
  * Force-bump frameworks internal b-d to 5.88.

 -- Norbert Preining <norbert@preining.info>  Wed, 17 Nov 2021 06:07:39 +0900

kdesignerplugin (5.86.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.86.0).
  * Bump Standards-Version to 4.6.0, no change required.

 -- Norbert Preining <norbert@preining.info>  Mon, 13 Sep 2021 10:53:20 +0900

kdesignerplugin (5.85.0-2) unstable; urgency=medium

  * Release to unstable.

 -- Norbert Preining <norbert@preining.info>  Sat, 28 Aug 2021 23:42:46 +0900

kdesignerplugin (5.85.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.85.0).

 -- Norbert Preining <norbert@preining.info>  Mon, 16 Aug 2021 00:02:26 +0900

kdesignerplugin (5.83.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Norbert Preining <norbert@preining.info>  Mon, 16 Aug 2021 11:53:02 +0900

kdesignerplugin (5.83.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.83.0).
  * Drop cross-compilation patch.

 -- Norbert Preining <norbert@preining.info>  Sun, 13 Jun 2021 10:34:20 +0900

kdesignerplugin (5.82.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.82.0).
  * Removed Maximiliano Curia from the uploaders, thanks for your work
    on the package!

 -- Norbert Preining <norbert@preining.info>  Sat, 08 May 2021 21:09:22 +0900

kdesignerplugin (5.81.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream version (5.81.0)
  * Bump inter-frameworks dependencies.

 -- Norbert Preining <norbert@preining.info>  Mon, 12 Apr 2021 10:18:17 +0900

kdesignerplugin (5.80.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream version (5.80.0)
  * Bump inter-frameworks dependencies.

 -- Norbert Preining <norbert@preining.info>  Mon, 15 Mar 2021 15:10:33 +0900

kdesignerplugin (5.79.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.79.0).

 -- Norbert Preining <norbert@preining.info>  Tue, 16 Feb 2021 06:45:27 +0900

kdesignerplugin (5.78.0-2) unstable; urgency=medium

  * Release to unstable.

 -- Norbert Preining <norbert@preining.info>  Sun, 17 Jan 2021 12:02:20 +0900

kdesignerplugin (5.78.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.78.0).
  * Update build-deps and deps with the info from cmake.

 -- Norbert Preining <norbert@preining.info>  Wed, 13 Jan 2021 10:32:41 +0900

kdesignerplugin (5.77.0-2) unstable; urgency=medium

  * Release to unstable.

 -- Norbert Preining <norbert@preining.info>  Tue, 22 Dec 2020 10:33:34 +0900

kdesignerplugin (5.77.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * Bump Standards-Version to 4.5.1 (No changes needed).
  * New upstream release (5.77.0).
  * Update build-deps and deps with the info from cmake.
  * Add myself to Uploaders.
  * Update maintainer to Debian Qt/KDE Maintainers.

 -- Norbert Preining <norbert@preining.info>  Fri, 18 Dec 2020 10:03:53 +0900

kdesignerplugin (5.74.0-2) unstable; urgency=medium

  * Team upload to unstable.

 -- Sandro Knauß <hefee@debian.org>  Mon, 19 Oct 2020 23:16:59 +0200

kdesignerplugin (5.74.0-1) experimental; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * New upstream release (5.74.0).
  * Update build-deps and deps with the info from cmake.
  * Update Homepage link to point to new invent.kde.org
  * Update repository related entries to metadata file.
  * Update field Source in debian/copyright to invent.kde.org move.
  * Add Bug-* entries to metadata file.

 -- Sandro Knauß <hefee@debian.org>  Sun, 27 Sep 2020 17:45:44 +0200

kdesignerplugin (5.70.0-1) unstable; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * New upstream release (5.70.0).
  * Update build-deps and deps with the info from cmake.
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Remove not needed Build-Depends.

 -- Sandro Knauß <hefee@debian.org>  Tue, 26 May 2020 23:57:05 +0200

kdesignerplugin (5.69.0-1) experimental; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * Bump compat level to 13.
  * Bump Standards-Version to 4.5.0 (No changes needed).
  * Get rid of debug-symbol-migration package.
  * Add Rules-Requires-Root field to control.
  * New upstream release (5.69.0).
  * Update build-deps and deps with the info from cmake.
  * Update patch hunks.
  * Remove not needed injection of linker flags.
  * Enable hardening all.
  * Cleanup copyright file.

 -- Sandro Knauß <hefee@debian.org>  Thu, 30 Apr 2020 00:53:10 +0200

kdesignerplugin (5.62.0-1) unstable; urgency=medium

  * Update reprotest args variable
  * Update watch file
  * New upstream release (5.62.0).
  * Update build-deps and deps with the info from cmake
  * Remove pics as done upstream
  * Drop the designer plugin (kf5widgets)
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Fri, 20 Sep 2019 10:12:58 -0700

kdesignerplugin (5.61.0-1) experimental; urgency=medium

  [ Maximiliano Curia ]
  * Salsa CI automatic initialization by Tuco
  * New upstream release (5.61.0).
  * Update build-deps and deps with the info from cmake
  * Update install files
  * Ignore reprotest build path variations
  * Release to experimental

  [ Jonathan Riddell ]
  * webkit is gone

 -- Maximiliano Curia <maxy@debian.org>  Mon, 09 Sep 2019 21:10:24 +0100

kdesignerplugin (5.54.0-1) unstable; urgency=medium

  * New upstream release (5.52.0).
  * Update build-deps and deps with the info from cmake
  * New upstream release (5.53.0).
  * Update build-deps and deps with the info from cmake
  * New upstream release (5.54.0).
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (5.54)
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Thu, 17 Jan 2019 19:26:40 -0300

kdesignerplugin (5.51.0-1) unstable; urgency=medium

  [ Maximiliano Curia ]
  * New revision
  * New upstream release (5.50.0).
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (5.50)
  * New upstream release (5.51.0).
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (5.51)
  * Release to unstable

  [ Helmut Grohne ]
  * Fix FTCBFS: Use the system kgendesignerplugin for cross building
    (Closes: 906966)

 -- Maximiliano Curia <maxy@debian.org>  Wed, 07 Nov 2018 17:16:41 +0100

kdesignerplugin (5.49.0-1) unstable; urgency=medium

  * New upstream release (5.48.0).
  * Update build-deps and deps with the info from cmake
  * New upstream release (5.49.0).
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (5.49)
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Fri, 17 Aug 2018 16:18:21 +0200

kdesignerplugin (5.47.0-1) unstable; urgency=medium

  * New upstream release (5.47.0).
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (5.47)
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Fri, 15 Jun 2018 12:08:50 +0200

kdesignerplugin (5.46.0-1) unstable; urgency=medium

  * New upstream release (5.46.0).
  * Update build-deps and deps with the info from cmake
  * Bump Standards-Version to 4.1.4.
  * Bump group breaks (5.46)
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Thu, 17 May 2018 22:14:45 +0200

kdesignerplugin (5.45.0-3) unstable; urgency=medium

  * Bump group breaks (5.45)
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Sat, 05 May 2018 08:10:17 +0200

kdesignerplugin (5.45.0-2) experimental; urgency=medium

  * New revision
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Fri, 27 Apr 2018 14:06:20 +0200

kdesignerplugin (5.45.0-1) experimental; urgency=medium

  * New upstream release (5.45.0).
  * Update build-deps and deps with the info from cmake
  * Split cmake files to a separate package (Closes: 893492)
  * Update copyright information
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Mon, 16 Apr 2018 22:03:09 +0200

kdesignerplugin (5.44.0-1) sid; urgency=medium

  [ Maximiliano Curia ]
  * New upstream release (5.44.0).
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (5.44)
  * Release to sid

  [ Helmut Grohne ]
  * Mark kgendesignerplugin Multi-Arch: foreign (Closes: 893492)

 -- Maximiliano Curia <maxy@debian.org>  Wed, 21 Mar 2018 14:47:31 +0100

kdesignerplugin (5.43.0-1) experimental; urgency=medium

  * Use the salsa canonical urls
  * New upstream release (5.43.0).
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (5.43)
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Mon, 26 Feb 2018 11:43:21 +0100

kdesignerplugin (5.42.0-3) sid; urgency=medium

  * Fix FTBFS on architectures where Qt uses OpenGL ES

 -- Maximiliano Curia <maxy@debian.org>  Wed, 14 Feb 2018 08:55:50 +0100

kdesignerplugin (5.42.0-2) sid; urgency=medium

  * New revision
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Sat, 10 Feb 2018 11:28:43 +0100

kdesignerplugin (5.42.0-1) experimental; urgency=medium

  * New upstream release (5.42.0).
  * Add link options as-needed
  * Bump debhelper build-dep and compat to 11.
  * Build without build_stamp
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (5.42)
  * Bump Standards-Version to 4.1.3.
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Fri, 02 Feb 2018 12:04:59 +0100

kdesignerplugin (5.41.0-1) experimental; urgency=medium

  * New upstream release (5.41.0).
  * Bump Standards-Version to 4.1.2.
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (5.41)
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Fri, 15 Dec 2017 10:42:25 -0300

kdesignerplugin (5.41.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 08 Dec 2017 14:45:12 +0000

kdesignerplugin (5.40.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 10 Nov 2017 12:57:03 +0000

kdesignerplugin (5.39.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sun, 15 Oct 2017 11:41:02 +0000

kdesignerplugin (5.38.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 11 Sep 2017 13:24:16 +0000

kdesignerplugin (5.37.0-2) sid; urgency=medium

  * New revision
  * Bump Standards-Version to 4.1.0.
  * Update upstream metadata
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Sun, 03 Sep 2017 09:57:56 +0200

kdesignerplugin (5.37.0-1) experimental; urgency=medium

  * New upstream release (5.37.0).
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (5.37)
  * Bump Standards-Version to 4.0.1.
  * wrap-and-sort
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Wed, 16 Aug 2017 10:43:56 +0200

kdesignerplugin (5.37.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 11 Aug 2017 15:12:51 +0000

kdesignerplugin (5.36.0-1) experimental; urgency=medium

  [ Maximiliano Curia ]
  * New upstream release (5.35.0).
  * Bump Standards-Version to 4.0.0.
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (5.35)
  * watch: Use https uris
  * New upstream release (5.36.0).
  * Bump group breaks (5.36)
  * Update build-deps and deps with the info from cmake

  [ Raymond Wooninck ]
  * Add signing key
  * also include signing-key

 -- Maximiliano Curia <maxy@debian.org>  Sun, 09 Jul 2017 23:42:44 +0200

kdesignerplugin (5.36.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 10 Jul 2017 17:10:18 +0000

kdesignerplugin (5.35.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sun, 11 Jun 2017 20:36:26 +0000

kdesignerplugin (5.34.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sun, 14 May 2017 17:35:29 +0000

kdesignerplugin (5.33.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 07 Apr 2017 16:22:06 +0000

kdesignerplugin (5.32.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sun, 12 Mar 2017 13:21:40 +0000

kdesignerplugin (5.31.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 13 Feb 2017 14:35:38 +0000

kdesignerplugin (5.30.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 16 Jan 2017 13:39:18 +0000

kdesignerplugin (5.29.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 09 Dec 2016 19:06:50 +0000

kdesignerplugin (5.28.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

  [ Maximiliano Curia ]
  * New upstream release (5.28)
  * Bump group breaks (5.28)

 -- Maximiliano Curia <maxy@debian.org>  Fri, 18 Nov 2016 16:07:17 +0100

kdesignerplugin (5.28.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 17 Nov 2016 09:44:44 +0000

kdesignerplugin (5.27.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

  [ Maximiliano Curia ]
  * New upstream release (5.27)
  * Bump group breaks (5.27)

 -- Maximiliano Curia <maxy@debian.org>  Sat, 15 Oct 2016 17:08:46 +0200

kdesignerplugin (5.27.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sat, 08 Oct 2016 11:29:57 +0000

kdesignerplugin (5.26.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

  [ Harald Sitter ]
  * fix typo

  [ Maximiliano Curia ]
  * Bump group breaks (5.26)

 -- Maximiliano Curia <maxy@debian.org>  Thu, 29 Sep 2016 12:03:05 +0200

kdesignerplugin (5.26.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 12 Sep 2016 08:30:23 +0000

kdesignerplugin (5.25.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

  [ Maximiliano Curia ]
  * Bump group breaks (5.25)

 -- Maximiliano Curia <maxy@debian.org>  Sat, 20 Aug 2016 16:47:57 +0200

kdesignerplugin (5.25.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sat, 13 Aug 2016 20:15:49 +0000

kdesignerplugin (5.24.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 11 Jul 2016 07:41:56 +0000

kdesignerplugin (5.23.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

 -- Maximiliano Curia <maxy@debian.org>  Thu, 23 Jun 2016 10:03:19 +0200

kdesignerplugin (5.23.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 13 Jun 2016 09:38:51 +0000

kdesignerplugin (5.22.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake
  * Bump Standards-Version to 3.9.8

  [ Maximiliano Curia ]
  * uscan no longer supports more than one main upstream tarball being listed

 -- Maximiliano Curia <maxy@debian.org>  Thu, 26 May 2016 10:55:36 +0200

kdesignerplugin (5.22.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 17 May 2016 07:27:39 +0000

kdesignerplugin (5.21.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 11 Apr 2016 09:21:39 +0000

kdesignerplugin (5.20.0-0neon) wily; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 01 Apr 2016 11:05:37 +0000

kdesignerplugin (5.19.0-1) experimental; urgency=medium

  * New upstream release (5.19.0).

 -- Maximiliano Curia <maxy@debian.org>  Sat, 13 Feb 2016 15:15:49 +0100

kdesignerplugin (5.18.0-1) experimental; urgency=medium

  * New upstream release (5.17.0).
  * New upstream release (5.18.0).

 -- Maximiliano Curia <maxy@debian.org>  Wed, 27 Jan 2016 13:33:20 +0100

kdesignerplugin (5.16.0-1) unstable; urgency=medium

  * New upstream release (5.16.0).

 -- Maximiliano Curia <maxy@debian.org>  Mon, 30 Nov 2015 12:12:21 +0100

kdesignerplugin (5.15.0-1) unstable; urgency=medium

  * New upstream release (5.15.0).

 -- Maximiliano Curia <maxy@debian.org>  Fri, 09 Oct 2015 19:15:51 +0200

kdesignerplugin (5.15.0-0ubuntu1) wily; urgency=medium

  [ Scarlett Clark ]
  * Vivid backport

  [ Jonathan Riddell ]
  * new upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Wed, 07 Oct 2015 13:46:49 +0100

kdesignerplugin (5.14.0-1) unstable; urgency=medium

  * New upstream release (5.14.0).

 -- Maximiliano Curia <maxy@debian.org>  Tue, 15 Sep 2015 13:47:39 +0200

kdesignerplugin (5.14.0-0ubuntu1) wily; urgency=medium

  * new upstream release

 -- Clive Johnston <clivejo@aol.com>  Thu, 17 Sep 2015 13:13:36 +0100

kdesignerplugin (5.13.0-1) unstable; urgency=medium

  * New upstream release (5.13.0).

 -- Maximiliano Curia <maxy@debian.org>  Tue, 01 Sep 2015 20:40:31 +0200

kdesignerplugin (5.13.0-0ubuntu1) wily; urgency=medium

  * new upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Mon, 10 Aug 2015 12:58:08 +0200

kdesignerplugin (5.12.0-1) unstable; urgency=medium

  * New upstream release (5.12.0).

 -- Maximiliano Curia <maxy@debian.org>  Thu, 09 Jul 2015 12:42:50 +0200

kdesignerplugin (5.12.0-0ubuntu1) wily; urgency=medium

  * New upstream release
  * Fix merge, remove unused broken backport.
  * Vivid backport.

 -- Scarlett Clark <sgclark@kubuntu.org>  Mon, 03 Aug 2015 14:28:10 +0200

kdesignerplugin (5.11.0-1) unstable; urgency=medium

  * New upstream release (5.10.0).
  * New upstream release (5.11.0).

 -- Maximiliano Curia <maxy@debian.org>  Mon, 29 Jun 2015 13:46:51 +0200

kdesignerplugin (5.10.0-0ubuntu1) wily; urgency=medium

  [ Maximiliano Curia ]
  * New upstream release (5.10.0).

  [ Jonathan Riddell ]
  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Wed, 03 Jun 2015 21:00:02 +0200

kdesignerplugin (5.9.0-1) experimental; urgency=medium

  * New upstream release (5.9.0).

 -- Maximiliano Curia <maxy@debian.org>  Thu, 23 Apr 2015 08:25:06 +0200

kdesignerplugin (5.9.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Mon, 13 Apr 2015 22:26:50 +0200

kdesignerplugin (5.8.0-1) experimental; urgency=medium

  * New upstream release (5.8.0).
  * Update copyright information.

 -- Maximiliano Curia <maxy@debian.org>  Sun, 22 Mar 2015 11:38:43 +0100

kdesignerplugin (5.8.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Tue, 17 Mar 2015 15:36:35 +0100

kdesignerplugin (5.7.0-1) experimental; urgency=medium

  * New upstream release (5.7.0).

 -- Maximiliano Curia <maxy@debian.org>  Fri, 06 Mar 2015 22:57:43 +0100

kdesignerplugin (5.7.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Harald Sitter <sitter@kde.org>  Tue, 10 Feb 2015 16:35:23 +0100

kdesignerplugin (5.6.0-1) experimental; urgency=medium

  * Prepare initial Debian release.
  * Update build dependencies to build against experimental and to
    follow cmake.
  * Bump Standards-Version to 3.9.6, no changes needed.
  * Update copyright information.
  * Update install files.
  * Update watch file.

 -- Maximiliano Curia <maxy@debian.org>  Wed, 04 Feb 2015 10:11:35 +0100

kdesignerplugin (5.6.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Tue, 06 Jan 2015 19:56:12 +0100

kdesignerplugin (5.5.0-0ubuntu1) vivid; urgency=medium

  [ Jonathan Riddell ]
  * Remove unused patch kubuntu_qtwebkit-5.1.diff

  [ Scarlett Clark ]
  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Mon, 15 Dec 2014 11:11:21 +0100

kdesignerplugin (5.4.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Fri, 07 Nov 2014 15:08:19 +0100

kdesignerplugin (5.3.0-0ubuntu1) utopic; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Tue, 07 Oct 2014 11:17:37 +0100

kdesignerplugin (5.2.0-0ubuntu1) utopic; urgency=medium

  * New upstream release
  * Use pkg-kde-tools version 3 scripts

 -- Jonathan Riddell <jriddell@ubuntu.com>  Mon, 22 Sep 2014 19:53:41 +0200

kdesignerplugin (5.1.0-0ubuntu1) utopic; urgency=medium

  * Update copyright to match licensing to source
  * New upstream release
  * Fix copyright file lintain error.

 -- Scarlett Clark <scarlett@scarlettgatelyclark.com>  Tue, 05 Aug 2014 17:28:18 +0200

kdesignerplugin (5.0.0-0ubuntu1) utopic; urgency=medium

  * Initial stable upstream release

 -- Scarlett Clark <scarlett@scarlettgatelyclark.com>  Tue, 15 Jul 2014 13:10:43 +0200
